# Vozão Digital - Partner REST API

## Descrição Geral

API REST para integração de sites ou serviços de parceiros do CearáSC com o App Vozão Digital.

Para obter credências de autenticação e URL do serviço, solicitar ao responsável (adminitrador de TI do Ceará Sport Club). Encaminhar também os IPs que serão utilizados para acessar a API.

## HTTP Response Codes da API

  - 200 - Sucesso. Verificar o JSON do body para detalhes.
  - 400 - Requisição inválida. Verificar parâmetros enviados.
  - 401 - Não autorizado. Credências inválidas ou não informadas.
  - 403 - Accesso negado. Credências válidas, porém não autorizadas para acessar o recurso.
  - 404 - Recurso não encontrado.
  - 500 - Erro, verificar o JSON do body para detalhes.
 
## Autenticação

- ``POST`` - ``/partner/token``
  ##### Request
  ```
  {
    "service": "[PartnerServiceId]",
    "login": "[PartnerUserLogin]",
    "password": "[PartnerUserPassword]"
  }
  ```
  ##### Response
  ```
  {
    "accessToken": "[Token]",
    "refreshToken": "[Token]"
  }
  ```
  AccessToken retornado deverar ser utilizado em todas as chamadas da API, no header ``Authorization`` com o valor ``Bearer [accessToken]``.

  Quando servidor retornar ``401 Unauthorized`` deve-se solicitar novo token de acesso utilizando o refreshToken.

***
- ``POST`` - ``/partner/token/refresh``
  ##### Request
  ```
  {
    "service": "[PartnerServiceId]",
    "login": "[PartnerUserLogin]",
    "password": "[PartnerUserPassword]"
  }
  ```
  ##### Response: ``OK``
  ```
  {

    "accessToken": "[Token]",
    "refreshToken": "[Token]"
  }
  ```

## Enviando Push Notification
- ``POST`` - ``/partner/push-notification/send``
  ##### Request
  ```
  {
    "to": {
      "broadcastTopic": "[broadcastTopic]",
      "userDocument": "[userDocument]",
      "userEmail": "[userEmail]"
    }
    "title": "[title]",
    "body": "[body]",
    "schedule": "[schedule]"
  }
  ```
  - **to**: Destinatário da notificação. Informar apenas um dos campos abaixo:
    - broadcastTopic: Tópico de broadcast para envio da notificação. Ex: ``all``, ``android``, ``ios`` (a lista de tópicos disponíveis deverá ser obtida com adminitrador de TI do Ceará Sport Club).
    - userDocument: CPF do usuário para envio da notificação.
    - userEmail: E-mail do usuário para envio da notificação.

  - **title**: Título da notificação.
  - **body**: Corpo da notificação.
  - schedule: Opcional, data e hora para envio da notificação. Formato: ``yyyy-MM-ddTHH:mm:ssZ`` - ISO 8601. Se não informado ou seja uma data passada a notificação será enviada imediatamente.

  ##### Response: ``OK``
  ```
  {
    "success": true
  }
  ```
